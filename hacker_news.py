#!/usr/bin/python
from xml.dom import minidom
import urllib2 

def xmlToText():
    count = len(titles) - 2
    while (count > -1):
        titles[count] = titles[count].firstChild.nodeValue
        links[count] = links[count].firstChild.nodeValue
        descriptions[count] = descriptions[count].firstChild.nodeValue
        comments[count] = comments[count].firstChild.nodeValue
        count = count - 1

def returnURL(URL):
    r = urllib2.urlopen(URL)
    data = r.read()
    r.close()
    return data

def getArticles(number):
    number = number - 1
    title = titles[number]
    description = descriptions[number]
    link = links[number]
    comment = comments[number]
    submenu=True
    while submenu:
        print ("""
    Title:            %s
    Link / GOTO:      %s
    Comments:         %s
        """ % (title, link, comment))
        try:
            # If http is in the description that means its a link post and
            # that it's likely that the description and link are identical
            # Otherwise it might be a text based post in which case the
            # description would be relevant
            if "http" not in description:
                print("        Description:      %s" % description)
            submenu=input("What would you like to read?\n" \
                          "Type 1 to view the artice, type 2 to see the "\
                          "comments\n" \
                          "Type 3 to go back to the main menu\n\n$  ")
            if submenu==1:
                getPage(link)
            elif submenu==2:
                getComments(comment)
            elif submenu==3:
                getMenu()
            elif submenu != "":
                print("\n Select a valid choice.")
            else:
                print("\n Select a valid choice.")
        except:
            print("\nThat's enough news today.")
            exit()


def getPage(URL):
    print("\nFeature still to be implemented")
    exit()

def getComments(URL):
    print("\nFeature still to be implemented")
    exit()

def getMenu():
    menu = True
    while menu:
        # The RSS feed has 30 headlines, after three tries this should throw
        # an exception that the list index is out of range in the below print
        # statement. 
        try:
            print ("""
    1.  %s
    2.  %s
    3.  %s
    4.  %s
    5.  %s
    6.  %s
    7.  %s
    8.  %s
    9.  %s
    10. %s
            """ % (titles[0],titles[1],titles[2],titles[3],titles[4],\
                   titles[5],titles[6],titles[7],titles[8],titles[9]))
            menu=input("What would you like to read?\n"\
                       "...for the next 10 headlines enter 0\n"\
                       "To exit type 'exit'\n\n$  ")
        except:
            print("\nThat's all the news today.")
            exit()
        if 0 < menu < 11:
            getArticles(menu)
            menu=False
            news=True
        elif menu==0:
            refreshMenu()
        elif menu=="exit":
            exit()
        elif menu !="":
            print("\n Not Valid Choice Try again")

def refreshMenu():
    for i in range(0, 9):
        titles.pop(0)
        links.pop(0)
        descriptions.pop(0)
        comments.pop(0)
    getMenu()



page = returnURL("https://news.ycombinator.com/rss")
xmldoc = minidom.parseString(page)
titles = xmldoc.getElementsByTagName('title')
links = xmldoc.getElementsByTagName('link')
descriptions = xmldoc.getElementsByTagName('description')
comments = xmldoc.getElementsByTagName('comments')
xmlToText()

getMenu()
