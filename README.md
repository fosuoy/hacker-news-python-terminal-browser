A quick and maybe messy terminal browser for ycombinator news.

The reason behind this is that having this script as an alias can make
accessing RSS headlines quickly.

Does not have many requirements / is just one single script.

Can also be easily altered to display on headlines elsewhere, for example a
desktop news ticker.

Requirements:
  Python 2.7.5
  Should not require any extra packages

// TODO
// Parse html links so that links can be read in terminal
// Parse comments so that top comments can be read in terminal
